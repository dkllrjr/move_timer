#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

while getopts t:e: option
do
    case "${option}"
        in
        t) SLEEP_TIME=${OPTARG}m;;
        e) TERM=${OPTARG};;
    esac
done

if [ "$SLEEP_TIME" == "" ]; then
    echo "Sleep time not specified!"
    exit
fi

while [ true ]; do
    sleep $SLEEP_TIME
    $TERM -e $DIR/notify.sh
done
