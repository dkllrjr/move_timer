#!/bin/bash

# Simple script that yells at you to move until you press a key

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

figlet -f doom "Get Up and Move!!!"

WAIT=2
TIMEOUT=300
LOOPS=$( expr $TIMEOUT / $WAIT )

for i in $( seq 1 1 $LOOPS ); do
    mpg123 -q $DIR/mike_UP_UP.mp3 &
    read -t $WAIT -n 1
    if [ $? = 0 ] ; then
        exit ;
    fi
done
